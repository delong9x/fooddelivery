$(document).ready(function(){
    tinymce.init({
        selector:'textarea.editor',
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#uploadImageContent').trigger('click');
                $('#uploadImageContent').on('change', function() {
                    var file = this.files[0];
                    var _token = $('input[name*="_token"]').val();
                    var data = {
                        file : file,
                        _token : _token
                    };
                    $.ajax({
                        url: "/api/upload/image",
                        dataUpload: data,
                        method: "POST",
                        processData: false,
                        success: function(result) {
                            console.log(result);
                        }
                    })
                });
            }
        },

    });
})