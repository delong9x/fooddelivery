<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Responses;
use Illuminate\Http\Request;
use App\Http\Helper;

/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 12/01/2017
 * Time: 3:29 PM
 */
class UploadImageController extends Controller
{
    public function Upload(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('ImageInput')) {
            $file = $request->file('ImageInput');
            $linkUploaded = Helper::UploadImage($file);
            $uploadImageResponse = new UploadImageResponse();
            $response = $uploadImageResponse->init();
            $response['Status'] = 'SUCCESS';
            $response['Message'] = 'Success upload!';
            $response['Data']['Name'] = $uploadImageResponse;
            $response['Links']['Self'] = env('APP_URL').$linkUploaded;

            return response()->json($response);
        } else {
            $response['Status'] = 'MISS_FILE';
            $response['Message'] = 'Cannot find image is uploaded!';

            return response()->json(["message" => "false to upload", "status" => 0]);
        }
    }
}