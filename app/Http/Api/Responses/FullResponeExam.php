<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 16/01/2017
 * Time: 4:50 PM
 */

namespace App\Http\Api\Response;


class FullResponseExam
{

    /*
        self: the link that generated the current response document.
        related: a related resource link when the primary data represents a resource relationship.
        pagination links for the primary data.
    */

    /* The value of the relationships key MUST be an object (a “relationships object”).
       Members of the relationships object (“relationships”) represent references from the resource object in which
       it’s defined to other resource objects.*/

    protected $__construct = array(
        'Type' => '', // using name model is recommended.
        'Status' => '', // Success or error or something else.
        'Message' => '', // string text for message
        'Links' => array('Self' => '', 'related' => ''), // a links object related to the primary data.
        'Relationships' => array(), // extent information success : author, creator, category,....
        'Data' => array(), // list attribute can be showed.
        'Included' => array() // an array of resource objects that are related to the primary data and/or each other (“included resources”).
    );

    public function init()
    {
        return $this->__construct;
    }
}