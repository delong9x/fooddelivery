<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 16/01/2017
 * Time: 4:47 PM
 */

namespace App\Http\Api\Responses;


class UploadImageResponse
{
    protected $__construct = array(
        'Type'=>'',
        'Status'=>'',
        'Message'=>'',
        'Links'=>array('Self'=> ''),
    );

    public function init(){
        return $this->__construct;
    }

}