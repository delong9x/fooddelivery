<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 9/01/2017
 * Time: 5:37 PM
 */

namespace App\Http;


class ViewHelper
{
    public static function RetrieveTreeOption($listTree){
        $html = "";
        foreach($listTree as $row) {
            $html .= "<option value='".$row['Id']."'>".$row['Title']."</option>";
            $html .= RetrieveChildNote($row);
        }

        return $html;
    }
    public function RetrieveChildNote($parentNote) {
        $html_str = "";
        if(count($parentNote['child']) > 0) {
            foreach($parentNote['child'] as $row) {
                $html_str .= "<option value='".$row['Id']."'>".$row['Title']."</option>";
                $html_str .= RetrieveChildNote($row);
            }
        }
        return $html_str;
    }
}