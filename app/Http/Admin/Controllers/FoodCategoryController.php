<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 4/01/2017
 * Time: 6:44 PM
 */

namespace App\Http\Admin\Controllers;


use App\Http\Helper;
use App\Http\Requests\Admin\FoodCategory\UpdateFoodCategoryForm;
use App\Http\Requests\Admin\FoodCategory\CreateFoodCategoryForm;
use App\Models\FoodCategory;
use Illuminate\Http\Request;
use Validator;

class FoodCategoryController extends Controller
{
    protected $listAssign = array();

    public function Index(Request $request)
    {
        // init model
        $foodCategory = new FoodCategory();

        // get list
        $listFoodCategory = $foodCategory->GetListFoodCategory(env('CountPerPage'));
        // assign to view
        $this->listAssign['ListFoodCategory'] = $listFoodCategory;
        return view('admin.foodcategory.index', $this->listAssign);
    }

    public function Create()
    {
        $foodCategory = new FoodCategory();
        $data = CreateFoodCategoryForm::InitForm();
        $treeCategory = $foodCategory->TreeFoodCategory();

        $treeCategory = Helper::RetrieveTreeOption($treeCategory);
        $this->listAssign['data'] = (object)$data;
        $this->listAssign['treeCategory'] = $treeCategory;
        return view('admin.foodcategory.create', $this->listAssign);
    }

    public function DoCreate(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = CreateFoodCategoryForm::rules();
            $data = CreateFoodCategoryForm::initForm();

            foreach ($data as $k=>$v) {
                if(isset($request->$k)) {
                    $data[$k] = $request->$k;
                }
            };

            $validate = Validator::make($data,$rules);
            if($validate->fails()) {
                $this->listAssign['data'] = (object)$data;
                return view('admin.administrator.create', $this->listAssign)->withErrors($validate, 'errors');
            } else {
                $data = (object)$data;

                $foodCategory = new FoodCategory();
                $newFoodCategory = $foodCategory->init();
                $newFoodCategory->Title = $data->Title;
                if($data->SeoUrl == "") {
                    $data->SeoUrl = Helper::makeSEOUrl($data->Title);
                }
                $newFoodCategory->Description = $data->Description;
                $newFoodCategory->MetaTitle = $data->MetaTitle;
                $newFoodCategory->MetaDescription = $data->MetaDescription;
                $newFoodCategory->ParentId = $data->ParentId;
                $newFoodCategory->IsActive = $data->IsActive;
                $saved = $newFoodCategory->save();
                if($saved == true) {
                    return redirect(route('ListFoodCategory'))->with('success', 'Create Admin Success');
                } else {
                    $this->listAssign['error'] = "Can not create new administrator, please try again!";
                    $this->listAssign['data'] = $data;
                    return view('admin.foodcategory.create', $this->listAssign)->withErrors($validate, 'errors');
                }
            }
        }

    }

    public function Update(Request $request, $id)
    {
        $foodCategory = new FoodCategory();
        $data = $foodCategory->GetFoodCategoryById($id);
        $treeCategory = $foodCategory->TreeFoodCategory($data->Id);
        $treeCategory = Helper::RetrieveTreeOption($treeCategory, $data->ParentId);
        if ($data != null) {
            $this->listAssign['data'] = $data;
            $this->listAssign['treeCategory'] = $treeCategory;
            return view('admin.foodcategory.update',$this->listAssign);
        } else {
            return redirect(route('ListFoodCategory'))->with( 'error','Your link is invalid');
        }

    }

    public function DoUpdate(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = UpdateFoodCategoryForm::rules();
            $data = UpdateFoodCategoryForm::initForm();
            $foodCategory = new FoodCategory();
            foreach ($data as $k=>$v) {
                $data[$k] = $request->$k;
            };

            $validate = Validator::make($data,$rules);
            if($validate->fails()) {
                $treeCategory = $foodCategory->TreeFoodCategory($data->Id);

                $treeCategory = Helper::RetrieveTreeOption($treeCategory);
                $this->listAssign['data'] = (object)$data;
                $this->listAssign['treeCategory'] = $treeCategory;
                return view('admin.foodcategory.update', $this->listAssign)->withErrors($validate, 'errors');
            } else {
                $data = (object)$data;
                $newFoodCategory = $foodCategory->GetFoodCategoryById($data->Id);
                $newFoodCategory->Title = $data->Title;
                $newFoodCategory->SeoUrl = $data->SeoUrl;
                if($data->SeoUrl == "") {
                    $data->SeoUrl = Helper::makeSEOUrl($data->Title);
                }
                $newFoodCategory->Description = $data->Description;
                $newFoodCategory->MetaTitle = $data->MetaTitle;
                $newFoodCategory->MetaDescription = $data->MetaDescription;
                $newFoodCategory->ParentId = $data->ParentId;
                $newFoodCategory->IsActive = $data->IsActive;
                $saved = $newFoodCategory->save();
                if($saved == true) {
                    return redirect(route('UpdateFoodCategory', ['id' => $newFoodCategory->Id]))->with('success', 'Update Admin Success');
                } else {
                    $this->listAssign['error'] = "Can not update category, please try again!";
                    $this->listAssign['data'] = $data;
                    return view('admin.foodcategory.update', $this->listAssign)->withErrors($validate, 'errors');
                }
            }
        }
    }

    public function Delete(Request $request, $id)
    {
        $foodCategory = new FoodCategory();
        $detailCategory = $foodCategory->GetFoodCategoryById($id);
        $detailCategory->IsDeleted = true;
        if($detailCategory->save()) {
            return redirect(route('ListFoodCategory'))->with( 'success','Success delete food category!');
        } else {
            return redirect(route('ListFoodCategory'))->with( 'error','There are some error while processing, please try again!');
        }
    }

    public function Detail(Request $request)
    {
        return view('admin.administrator.create');
    }
}