<?php

namespace App\Http\Admin\Controllers\Auth;

use App\Http\Admin\Controllers\Controller;
use App\Http\Requests\Admin\Auth\LoginForm;
use App\Models\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $listAssign = array();

    public function Index(){
        $data = LoginForm::InitForm();
        $this->listAssign['data'] = (object)$data;
        return view('admin.auth.login', $this->listAssign);
    }
    public function DoLogin(Request $request){
        $method = $request->method();
        if($method == "POST") {
            $rules = LoginForm::rules();
            $data = LoginForm::InitForm();
            foreach ($data as $k=>$v) {
                $data[$k] = $request->$k;
            };
            $validate = Validator::make($data,$rules);
            if($validate->fails()) {
                $this->listAssign['data'] = (object)$data;
                return view('admin.auth.login', $this->listAssign)->withErrors($validate, 'errors');
            } else {

                $administrator = new Authenticate();
                $data = (object)$data;
                $result = $administrator->GetAdminByUsername($data->Username);
                if($result == null) {
                    $this->listAssign['error'] = "Username or password is not match!";
                    return view('admin.auth.login', $this->listAssign);
                } else {
                    $password = md5($data->Password);
                    if ($password == $result->Password) {
                        if($data->IsRemember == 1) {
                            Auth::loginUsingId($result->Id, true);
                        } else {
                            Auth::loginUsingId($result->Id);
                        }
                        Session::put('User', $result);
                        Session::save();
                        return redirect(route('HomePanel'))->with(['success'=>'Login Success!']);
                    } else {
                        $this->listAssign['error'] = "Username or password is not match!";
                        return view('admin.auth.login', $this->listAssign);
                    }
                }
            }
        }
    }
}
