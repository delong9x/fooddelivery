<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 4/01/2017
 * Time: 6:44 PM
 */

namespace App\Http\Admin\Controllers;


use App\Http\Requests\Admin\Food\CreateFoodForm;
use App\Http\Requests\Admin\Food\SearchFoodForm;
use App\Models\Food;
use App\Models\FoodCategory;
use Illuminate\Http\Request;
use App\Http\Helper;
use Validator;

class FoodController extends Controller
{

    protected $listAssign = array();


    /**
     * Show list
     *
     */
    public function Index(Request $request)
    {
        // init model
        $Food = new Food();

        // search
        $searchForm = SearchFoodForm::InitForm();
        $listRequest = $request->request->all();
        foreach ($searchForm as $k => $v) {
            if(isset($listRequest[$k])) {
                $searchForm[$k] = $listRequest[$k];
            }

        };
        // get list
        $listFood = $Food->GetListFood(env('CountPerPage'), $searchForm);
        // assign to view
        $this->listAssign['listFood'] = $listFood;
        $this->listAssign['searchForm'] = (object)$searchForm;
        return View('admin.food.index', $this->listAssign);
    }

    public function Create()
    {
        $foodCategory = new FoodCategory();
        $treeCategory = $foodCategory->TreeFoodCategory(0);
        $treeCategory = Helper::RetrieveTreeOption($treeCategory, 0);
        $data = CreateFoodForm::InitForm();
        $this->listAssign['treeCategory'] = $treeCategory;
        $this->listAssign['data'] = (object)$data;
        return view('admin.food.create', $this->listAssign);
    }

    public function DoCreate(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            //
        }
    }

    public function Update(Request $request,$id)
    {
        return view('admin.food.update');
    }

    public function DoUpdate(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            //

            return view('admin.food.update');
        } else {
            redirect()->route('HomePanel')->with('error', 'Error, Please Check again');
        }
    }

    public function Delete(Request $request, $id)
    {
        $food = new Food();
        $foodDetail = $food->GetFoodById($id);
        $foodDetail->IsDeleted = true;
        if($foodDetail->save()) {
            return redirect(route('ListFood'))->with( 'success','Success delete food!');
        } else {
            return redirect(route('ListFood'))->with( 'error','There are some error while processing, please try again!');
        }
    }

    public function Detail(Request $request){

    }
}