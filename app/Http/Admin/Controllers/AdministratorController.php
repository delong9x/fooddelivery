<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 4/01/2017
 * Time: 6:44 PM
 */

namespace App\Http\Admin\Controllers;


use App\Http\Helper;
use App\Http\Requests\Admin\Administrator\SearchAdminForm;
use App\Http\Requests\Admin\Administrator\UpdateAdminForm;
use App\Models\Administrator;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Administrator\CreateAdminForm;
use Validator;

class AdministratorController extends Controller
{

    protected $listAssign = array();

    public function Index(Request $request)
    {
        // init model
        $Administrator = new Administrator();

        // search
        $searchForm = SearchAdminForm::InitForm();
        $listRequest = $request->request->all();
        foreach ($searchForm as $k => $v) {
            if(isset($listRequest[$k])) {
                $searchForm[$k] = $listRequest[$k];
            }

        };
        // get list
        $listAdmin = $Administrator->getListAdmin(env('CountPerPage'), $searchForm);
        // assign to view
        $this->listAssign['ListAdmin'] = $listAdmin;
        $this->listAssign['SearchForm'] = (object)$searchForm;
        return view('admin.administrator.index', $this->listAssign);
    }

    public function Create()
    {
        $data = CreateAdminForm::InitForm();
        $this->listAssign['data'] = (object)$data;
        return view('admin.administrator.create', $this->listAssign);
    }

    public function DoCreate(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = CreateAdminForm::rules();
            $data = CreateAdminForm::initForm();

            foreach ($data as $k=>$v) {
                $data[$k] = $request->$k;
            };
            $validate = Validator::make($data,$rules);
            if($validate->fails()) {
                $this->listAssign['data'] = (object)$data;
                return view('admin.administrator.create', $this->listAssign)->withErrors($validate, 'errors');
            } else {
                $data = (object)$data;
                $Administrator = new Administrator();
                $newAdmin = $Administrator->init();
                $newAdmin->FirstName = $data->FirstName;
                $newAdmin->LastName = $data->LastName;
                $newAdmin->Username = $data->Username;
                $newAdmin->Password = md5($data->Password);
                $newAdmin->Phone = $data->Phone;
                $newAdmin->Address = $data->Address;
                $newAdmin->Email = $data->Email;

                if ($request->hasFile('AvatarInput')) {
                    $file = $request->file('AvatarInput');
                    Helper::UploadImage($file);
                }
                else
                {
                    $newAdmin->Avatar = "";
                }
                $saved = $newAdmin->save();
                if($saved == true) {
                    return redirect(route('ListAdmin'))->with('success', 'Create Admin Success');
                } else {
                    $this->listAssign['error'] = "Can not create new administrator, please try again!";
                    return view('admin.administrator.create', $this->listAssign)->withErrors($validate, 'errors');
                }
            }
        }

    }

    public function Update(Request $request, $id)
    {
        $Administrator = new Administrator();
        $data = $Administrator->GetAdminByID($id);
        if ($data != null) {
            $this->listAssign['data'] = $data;
            return view('admin.administrator.update',$this->listAssign);
        } else {
            return redirect(route('ListAdmin'))->with( 'error','Your link is invalid');
        }

    }

    public function DoUpdate(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = UpdateAdminForm::rules();
            $data = UpdateAdminForm::initForm();

            foreach ($data as $k=>$v) {
                $data[$k] = $request->$k;
            };

            $validate = Validator::make($data,$rules);
            if($validate->fails() || $data['Password'] != $data['Password_confirmation']) {
                $this->listAssign['data'] = (object)$data;
                $this->listAssign['error'] = 'Retype password is not matched';

                return view('admin.administrator.update', $this->listAssign)->withErrors($validate, 'errors');
            } else {
                $data = (object)$data;
                $Administrator = new Administrator();
                $admin = $Administrator->GetAdminByID($data->Id);
                $admin->FirstName = $data->FirstName;
                $admin->LastName = $data->LastName;
                $admin->Username = $data->Username;
                $admin->Phone = $data->Phone;
                $admin->Address = $data->Address;
                $admin->Email = $data->Email;
                if(!empty($data->Password)) {
                    $admin->Password = md5($data->Password);
                }

                if ($request->hasFile('AvatarInput')) {
                    $file = $request->file('AvatarInput');
                    $admin->Avatar = Helper::UploadImage($file);
                }
                else
                {
                    $admin->Avatar = $data->avatar_src;
                }
                $saved = $admin->save();
                if($saved == true) {
                    return redirect(route('UpdateAdmin', ['id' => $admin->Id]))->with('success', 'Update Admin Success');
                } else {
                    $this->listAssign['error'] = "Can not create new administrator, please try again!";
                    $this->listAssign['data'] = $data;
                    return view('admin.administrator.update', $this->listAssign)->withErrors($validate, 'errors');
                }
            }
        }
    }

    public function Delete(Request $request, $id)
    {
        $Administrator = new Administrator();
        $admin = $Administrator->GetAdminByID($id);
        $admin->IsDeleted = true;
        if($admin->save()) {
            return redirect(route('ListAdmin'))->with( 'success','Success delete administrator!');
        } else {
            return redirect(route('ListAdmin'))->with( 'error','There are some error while processing, please try again!');
        }
    }

    public function Detail(Request $request)
    {
        return view('admin.administrator.create');
    }
}