<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 4/01/2017
 * Time: 6:44 PM
 */

namespace App\Http\Admin\Controllers;


use Illuminate\Http\Request;
use App\Http\helper;

class BlankController extends Controller
{


    protected $listAssign = array();


    /**
     * Show list
     *
     */
    public function Index(Request $request)
    {

    }

    public function Create()
    {
        return view('admin.modulename.create');
    }

    public function DoCreate(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            //
        }
    }

    public function Update(Request $request,$id)
    {
        return view('admin.modulename.update');
    }

    public function DoUpdate(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            //

            return view('admin.modulename.update');
        } else {
            redirect()->route('HomePanel')->with('error', 'Error, Please Check again');
        }
    }

    public function Delete(Request $request)
    {

    }

    public function Detail(Request $request){

    }
}