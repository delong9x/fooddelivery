<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 4/01/2017
 * Time: 6:44 PM
 */

namespace App\Http\Admin\Controllers;


class GroupAdministratorController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function Index(Request $request)
    {

    }

    public function Create()
    {
        return view('admin.groupadmin.create');
    }

    public function DoCreate(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            //
        }
    }

    public function Update(Request $request,$id)
    {
        return view('admin.modulename.update');
    }

    public function DoUpdate(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            //

            return view('admin.groupadmin.update');
        } else {
            redirect()->route('HomePanel')->with('error', 'Error, Please Check again');
        }
    }

    public function Delete(Request $request)
    {

    }

    public function Detail(Request $request){

    }
}