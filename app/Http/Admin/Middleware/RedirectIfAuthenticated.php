<?php

namespace App\Http\Admin\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check() == false && Request::fullUrl() != route('AdminLogin')) {
            return redirect(route('AdminLogin'));
        }
        if (Auth::check() != false && Request::fullUrl() == route('AdminLogin')) {
            return redirect(route('HomePanel'));
        }

        return $next($request);
    }
}
