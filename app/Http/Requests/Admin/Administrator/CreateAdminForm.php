<?php

namespace App\Http\Requests\Admin\Administrator;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'FirstName' => 'required|min:2', // required, at least 2 characters
            'LastName' => 'required|min:2', // required, at least 2 characters
            'Username' => 'required|unique:administrator,UserName|min:4|alpha_num',                        // required, unique in table administrator, field UserName, at least 4 Characters and in alphabet
            'Email' => 'required|email|unique:administrator,Email',     // required and must be unique in the ducks table
            'Password' => 'required|min:4|alpha|confirmed' // required, in Alphabet ,need to be confirmed
        ];
    }

    public static function InitForm()
    {
        return [
            'Username' => '',
            'Password' => '',
            'Password_confirmation' => '',
            'FirstName' => '',
            'LastName' => '',
            'Email' => '',
            'Phone' => '',
            'Address' => '',
            'avatar_src' => '',
            'avatar_data' => '',
            'Avatar'=>'',
            'IsActive'=>1
        ];
    }


}
