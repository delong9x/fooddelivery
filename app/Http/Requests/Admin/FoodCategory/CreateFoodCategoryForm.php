<?php

namespace App\Http\Requests\Admin\FoodCategory;

use Illuminate\Foundation\Http\FormRequest;

class CreateFoodCategoryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'Title' => 'required|min:2', // required, at least 2 characters
            'Description' => 'required|min:2',                        // required, unique in table administrator, field UserName, at least 4 Characters and in alphabet
            'SeoUrl'=>'required|unique:food_category,SeoUrl|min:2'
        ];
    }

    public static function InitForm()
    {
        return [
            'SeoUrl'=>'',
            'Title'=>'',
            'Description'=>'',
            'MetaTitle'=>'',
            'MetaDescription'=>'',
            'foodNumber'=>0,
            'ParentId'=>0,
            'IsActive'=>true,
            'IsDeleted'=>false,
        ];
    }


}
