<?php

namespace App\Http\Requests\Admin\FoodCategory;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateFoodCategoryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'Id' => 'required',
            'Title' => 'required|min:2', // required, at least 2 characters
            'Description' => 'required|min:2',                        // required, unique in table administrator, field UserName, at least 4 Characters and in alphabet
        ];
    }

    public static function InitForm()
    {
        return [
            'Id' => '',
            'Title'=>'',
            'Description'=>'',
            'SeoUrl'=>'',
            'MetaTitle'=>'',
            'MetaDescription'=>'',
            'foodNumber'=>0,
            'ParentId'=>0,
            'IsActive'=>true,
            'UpdatedById'=>Auth::id(),
            'UpdatedAt'=>Carbon::now()
        ];
    }
}
