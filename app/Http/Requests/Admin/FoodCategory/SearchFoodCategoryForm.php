<?php

namespace App\Http\Requests\Admin\CreateFoodCategoryForm;

use Illuminate\Foundation\Http\FormRequest;

class SearchFoodCategoryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
        ];
    }

    public static function InitForm()
    {
        return  array('Title' => '', 'FoodCategoryId' => '','Description'=>'', 'IsActive' => '0');
    }


}
