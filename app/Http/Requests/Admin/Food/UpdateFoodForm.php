<?php

namespace App\Http\Requests\Admin\Food;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class UpdateFoodForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'FirstName' => 'required|min:2', // required, at least 2 characters
            'LastName' => 'required|min:2', // required, at least 2 characters
        ];
    }

    public static function InitForm()
    {
        return [
            'Id' => '',
            'Username' => '',
            'Password' => '',
            'Password_confirmation' => '',
            'FirstName' => '',
            'LastName' => '',
            'Email' => '',
            'Phone' => '',
            'Address' => '',
            'avatar_src' => '',
            'avatar_data' => '',
            'Avatar'=>'',
            'IsActive'=>1,
            'UpdatedAt'=>Carbon::now()->toDateTimeString()
        ];
    }
}
