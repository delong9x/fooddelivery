<?php

namespace App\Http\Requests\Admin\Food;

use Illuminate\Foundation\Http\FormRequest;

class CreateFoodForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'Title' => 'required|min:2', // required, at least 2 characters
            'Description' => 'required|min:2', // required, at least 2 characters
            'Image' => 'required',
            'Price' => 'required',
            // required, unique in table administrator, field UserName, at least 4 Characters and in alphabet
        ];
    }

    public static function InitForm()
    {
        return [
            'Title'=>'',
            'Description'=>'',
            'Content'=>'',
            'Image'=>'',
            'Photo'=>'',
            'Price'=>'',
            'PriceDown'=>'',
            'FoodCategoryId'=>0,
            'IsStock'=>0,
            'IsHomepage'=>1,
            'IsHot'=>0,
            'IsActive'=>0
        ];
    }


}
