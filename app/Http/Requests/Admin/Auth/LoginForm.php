<?php

namespace App\Http\Requests\Admin\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'Username' => 'required|min:4|alpha_num',                        // required, unique in table administrator, field UserName, at least 4 Characters and in alphabet
            'Password' => 'required|min:4|alpha' // required, in Alphabet ,need to be confirmed
        ];
    }

    public static function InitForm()
    {
        return [
            'Username' => '',
            'Password' => '',
            'IsRemember' => 0
        ];
    }


}
