<?php

namespace App\Http;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class Helper
{
    public static function UploadImage($input, $width = 300, $height = 300)
    {
        $imageUploaded = Image::make($input)->resize($width, $height);
        $filename = date("His") . rand(0, 20000);
        $directory = env('UploadDirectory') . '/' . date("Y/m/d");
        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775, true, true);
        }
        $imageUploaded->save($directory .'/'. $filename);
        return $directory .'/'. $filename;

    }

    public static function makeSEOUrl($str)
    {
        $a = array(
            'á', 'é', 'í', 'ó', 'ú', 'ý', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ý',
            'à', 'è', 'ì', 'ò', 'ù', 'ỳ', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Ỳ',
            'ả', 'ẻ', 'ỉ', 'ỏ', 'ủ', 'ỷ', 'Ả', 'Ẻ', 'Ỉ', 'Ỏ', 'Ủ', 'Ỷ',
            'ã', 'ẽ', 'ĩ', 'õ', 'ũ', 'ỹ', 'Ã', 'Ẽ', 'Ĩ', 'Õ', 'Ũ', 'Ỹ',
            'ạ', 'ẹ', 'ị', 'ọ', 'ụ', 'ỵ', 'Ạ', 'Ẹ', 'Ị', 'Ọ', 'Ụ', 'Ỵ',
            'â', 'ê', 'ô', 'ư', 'Â', 'Ê', 'Ô', 'Ư',
            'ấ', 'ế', 'ố', 'ứ', 'Ấ', 'Ế', 'Ố', 'Ứ',
            'ầ', 'ề', 'ồ', 'ừ', 'Ầ', 'Ề', 'Ồ', 'Ừ',
            'ẩ', 'ể', 'ổ', 'ử', 'Ẩ', 'Ể', 'Ổ', 'Ử',
            'ậ', 'ệ', 'ộ', 'ự', 'Ậ', 'Ệ', 'Ộ', 'Ự',
            ' '
        );
        $b = array(
            'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y',
            'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y',
            'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y',
            'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y',
            'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y',
            'a', 'e', 'o', 'u', 'A', 'E', 'O', 'U',
            'a', 'e', 'o', 'u', 'A', 'E', 'O', 'U',
            'a', 'e', 'o', 'u', 'A', 'E', 'O', 'U',
            'a', 'e', 'o', 'u', 'A', 'E', 'O', 'U',
            'a', 'e', 'o', 'u', 'A', 'E', 'O', 'U',
            '-'
        );
        return str_replace($a, $b, $str);
    }


    public static function RetrieveTreeOption($listTree, $currentId = 0)
    {
        $html = "";
        if (count($listTree) > 0) {
            foreach ($listTree as $row) {
                $html .= "<option";
                if($row["Id"] == $currentId) {
                    $html .= " selected ";
                };
                $html .= " value='" . $row['Id'] . "'>" . $row['Title'] . "</option>";
                $html .= self::RetrieveChildNote($row, $currentId);
            }
        }

        return $html;
    }

    public static function RetrieveChildNote($parentNote, $currentId, $levelRange = "---")
    {
        $html_str = "";
        if (count($parentNote['child']) > 0) {
            foreach ($parentNote['child'] as $row) {
                $html_str .= "<option";
                if($row["Id"] == $currentId) {
                    $html_str .= " selected ";
                };
                $html_str .= " value='" . $row['Id'] . "'>" .$levelRange." ". $row['Title'] . "</option>";
                $html_str .= self::RetrieveChildNote($row,$currentId, $levelRange.="---");
            }
        }
        return $html_str;
    }
}