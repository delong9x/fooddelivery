<?php
/**
 * Created by PhpStorm.
 * User: CharlieVu
 * Date: 4/01/2017
 * Time: 6:44 PM
 */

namespace App\Http\Website\Controllers;


class MenuController
{
    /**
     * Show the index for website
     *
     */
    public function index()
    {
        return view('website.menu');
    }
}