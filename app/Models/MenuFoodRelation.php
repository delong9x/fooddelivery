<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuFoodRelation extends Model {

    /**
     * Generated
     */

    protected $table = 'menu_food_relation';
    protected $fillable = ['Id', 'MenuId', 'FoodId', 'Ordering', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
