<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    /**
     * Generated
     */

    protected $table = 'order';
    protected $fillable = ['Id', 'CustomerId', 'CustomerName', 'CustomerEmail', 'CustomerPhone', 'Ordering', 'ShipperId', 'ShipDate', 'RequiredDate', 'Freight', 'SaleTax', 'Total', 'TransactionStatus', 'ErrorLog', 'ErrorMessenger', 'Fulfilled', 'IsFail', 'IsDeleted', 'isPaid', 'IsSell', 'CreatedAt', 'UpdatedAt'];

    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';

}
