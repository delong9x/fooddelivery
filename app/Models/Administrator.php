<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Administrator extends Model {

    /**
     * Generated
     */

    protected $table = 'administrator';
    protected $fillable = ['Id', 'FirstName', 'LastName', 'Username', 'Password', 'Address', 'City', 'State', 'PostalCode', 'Country', 'Phone', 'Email', 'Avatar', 'GroupId', 'LoginIp', 'LastLogin', 'IsActive', 'IsDeleted', 'CreatedAt', 'UpdatedAt','RememberToken'];
    protected $rememberTokenName = "RememberToken";
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';


    public function getListAdmin($countPerPage, $searchForm){
            $list = self::where('IsDeleted','<>',true)->OrderBy('Id', 'DESC');
            foreach($searchForm as $k=>$v){
                if(!empty($v)) {
                    if($v != 'true' && $v != 'false' && $v !='0') {
                        $list = $list->where($k, 'like', '%'.$v.'%');
                    } else if ($v != '0') {
                        $list = $list->where($k, '=', $v);
                    }
                }
            }
            $list = $list->OrderBy('Id', 'DESC')->paginate($countPerPage);
        return $list;
    }

    public static function init(){
        $newAdmin = new self;
        $newAdmin->IsActive = true;
        $newAdmin->IsDeleted = false;
        $newAdmin->CreatedAt = Carbon::now()->toDateTimeString();
        $newAdmin->UpdatedAt = Carbon::now()->toDateTimeString();
        return $newAdmin;
    }

    public function GetAdminByUsername($username){
        $admin = self::where("Username","=",$username)->where("IsDeleted","<>", 1)->first();
        return $admin;
    }

    public function GetAdminByID($id) {
        $admin = self::where("IsDeleted","<>", 1)->Find($id);
        return $admin;
    }



}
