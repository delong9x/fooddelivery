<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class FoodCategory extends Model {

    /**
     * Generated
     */

    protected $table = 'food_category';
    protected $fillable = ['Id', 'SeoUrl', 'Title', 'Description', 'MetaTitle', 'MetaDescription', 'foodNumber', 'ParentId', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

    public function GetListFoodCategory($countPerPage)
    {
        $list = self::Where('IsDeleted', '<>', true)->OrderBy('Id', 'DESC');
        $list = $list->OrderBy('Id', 'DESC')->Paginate($countPerPage);
        return $list;
    }

    public function GetFoodCategoryById($id){
        return self::Where('IsDeleted', '<>', true)->find($id);
    }

    public function TreeFoodCategory($rejected = 0){
        $root_items = self::Select('Id','Title')->Where('ParentId','=',0)->Where('IsDeleted', '<>', true)->Where("Id","<>",$rejected)->Get()->toArray();
        foreach($root_items as $k=>$v){
            $root_items[$k]['child'] = $this->GetChildren($v);
        }
        return $root_items;
    }

    protected function GetChildren($parent){
        $childItems = self::Select('Id','Title')->Where('ParentId','=',$parent['Id'])->Get()->toArray();
        if(count($childItems) == 0) return;
        else {
            foreach($childItems as $k=>$v) {
                $childItems[$k]['child'] = self::GetChildren($v);
            }
            return $childItems;
        }
    }
    public function init(){
         $newFoodCategory = new self;
        $newFoodCategory->SeoUrl = '';
         $newFoodCategory->Title = '';
         $newFoodCategory->Description = '';
         $newFoodCategory->MetaTitle = '';
         $newFoodCategory->MetaDescription = '';
         $newFoodCategory->foodNumber = 0;
        $newFoodCategory->ParentId = 0;
        $newFoodCategory->IsActive = 0;
        $newFoodCategory->IsDeleted = false;
        $newFoodCategory->CreatedAt = Auth::id();
        $newFoodCategory->UpdatedById = Auth::id();
        $newFoodCategory->CreatedAt = Carbon::now();
        $newFoodCategory->UpdatedAt = Carbon::now();
        return $newFoodCategory;
    }
}
