<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model {

    /**
     * Generated
     */

    protected $table = 'social';
    protected $fillable = ['Id', 'Name', 'ApiVersion', 'ClientId', 'Secret', 'Ordering', 'Image', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';
}
