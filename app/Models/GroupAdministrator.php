<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupAdministrator extends Model {

    /**
     * Generated
     */

    protected $table = 'group_administrator';
    protected $fillable = ['Id', 'Name', 'Image', 'Role', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
