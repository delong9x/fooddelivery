<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectionFoodRelation extends Model {

    /**
     * Generated
     */

    protected $table = 'collection_food_relation';
    protected $fillable = ['Id', 'FoodId', 'CollectionId', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
