<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    /**
     * Generated
     */

    protected $table = 'article';
    protected $fillable = ['Id', 'SeoUrl', 'Title', 'MetaTitle', 'MetaDescription', 'Description', 'Content', 'Image', 'YoutubeLink', 'ArticleRelated', 'FoodRelated', 'IsVideo', 'IsHomepage', 'IsHot', 'IsActive', 'IsDeleted', 'CloudTag', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
