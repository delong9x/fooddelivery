<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    /**
     * Generated
     */

    protected $table = 'comment';
    protected $fillable = ['Id', 'TargetId', 'Type', 'Title', 'Content', 'Name', 'Image', 'IsAdmin', 'UserID', 'IsDeleted', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
