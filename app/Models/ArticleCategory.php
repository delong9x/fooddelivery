<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model {

    /**
     * Generated
     */

    protected $table = 'article_category';
    protected $fillable = ['Id', 'Name', 'Description', 'Content', 'Ordering', 'ParentId', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
