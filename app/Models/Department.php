<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

    /**
     * Generated
     */

    protected $table = 'department';
    protected $fillable = ['Id', 'Name', 'Email', 'Phone', 'Ordering', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';

    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
}
