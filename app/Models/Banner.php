<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {

    /**
     * Generated
     */

    protected $table = 'banner';
    protected $fillable = ['Id', 'Image', 'Alt', 'Link', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];

    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
}
