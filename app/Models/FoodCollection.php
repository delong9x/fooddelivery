<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodCollection extends Model {

    /**
     * Generated
     */

    protected $table = 'food_collection';
    protected $fillable = ['Id', 'SeoUrl', 'Title', 'Description', 'MetaTitle', 'MetaDescription', 'FoodNumber', 'ParentId', 'StartTime', 'EndTime', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
