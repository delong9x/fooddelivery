<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    /**
     * Generated
     */

    protected $table = 'contact';
    protected $fillable = ['Id', 'Name', 'Email', 'Phone', 'Content', 'IsReplied', 'IsEmailDiscountReceiver', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';


}
