<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{

    /**
     * Generated
     */

    protected $table = 'food';
    protected $fillable = [
        'Id',
        'FoodCategoryId',
        'Title',
        'Description',
        'SubTitle',
        'SeoUrl',
        'MetaTitle',
        'MetaDescription',
        'Note',
        'Content',
        'Photo',
        'Image',
        'Price',
        'PriceDown',
        'ListColor',
        'ListSize',
        'RelatedFood',
        'RelatedArticle',
        'BuyNumber',
        'Ranking',
        'DiscountAvailable',
        'Quantify',
        'IsStock',
        'IsHomepage',
        'IsHot',
        'CloudTag',
        'IsActive',
        'IsDeleted',
        'CreatedById',
        'UpdatedById',
        'CreatedAt',
        'UpdatedAt'
    ];

    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

    public function GetListFood($countPerPage, $searchForm)
    {
        $list = self::where('IsDeleted', '<>', true)->OrderBy('Id', 'DESC');
        foreach ($searchForm as $k => $v) {
            if (!empty($v)) {
                if ($v != 'true' && $v != 'false' && $v != '0') {
                    $list = $list->where($k, 'like', '%' . $v . '%');
                } else if ($v != '0') {
                    $list = $list->where($k, '=', $v);
                }
            }
        }
        $list = $list->OrderBy('Id', 'DESC')->paginate($countPerPage);
        return $list;
    }

    public function GetFoodById($id)
    {
        return self::where('IdDeleted', "<>", 1)->find($id);
    }


}
