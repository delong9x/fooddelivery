<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    /**
     * Generated
     */

    protected $table = 'menu';
    protected $fillable = ['Id', 'Name', 'WeekDay', 'PositionCode', 'Ordering', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
