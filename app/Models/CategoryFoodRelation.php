<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFoodRelation extends Model {

    /**
     * Generated
     */

    protected $table = 'category_food_relation';
    protected $fillable = ['Id', 'FoodId', 'FoodCategoryId', 'IsActive', 'IsDeleted', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
