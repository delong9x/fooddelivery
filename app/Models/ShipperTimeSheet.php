<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipperTimeSheet extends Model {

    /**
     * Generated
     */

    protected $table = 'shipper_time_sheet';
    protected $fillable = ['Id', 'ShipperId', 'OrderId', 'StartTime', 'EndTime', 'IsRefuse', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';
}
