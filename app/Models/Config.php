<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model {

    /**
     * Generated
     */

    protected $table = 'config';
    protected $fillable = ['Id', 'Title', 'Key', 'Value', 'Type', 'IsFixed', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
