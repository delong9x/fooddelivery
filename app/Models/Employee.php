<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model {

    /**
     * Generated
     */

    protected $table = 'employee';
    protected $fillable = ['Id', 'Name', 'Email', 'Phone', 'Salary', 'DepartmentId', 'Rate', 'StartWorkAt', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];

    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
}
