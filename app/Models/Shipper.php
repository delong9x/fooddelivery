<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model {

    /**
     * Generated
     */

    protected $table = 'shipper';
    protected $fillable = ['Id', 'Name', 'Email', 'Phone', 'Salary', 'Rate', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';
}
