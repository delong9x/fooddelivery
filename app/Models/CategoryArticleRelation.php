<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryArticleRelation extends Model {

    /**
     * Generated
     */

    protected $table = 'category_article_relation';
    protected $fillable = ['Id', 'ArticleId', 'CategoryArticleId', 'CreatedAt', 'UpdatedAt'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
