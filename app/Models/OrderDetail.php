<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model {

    /**
     * Generated
     */

    protected $table = 'order_detail';
    protected $fillable = ['Id', 'OrderId', 'FoodId', 'OrderNumber', 'Price', 'Quantity', 'Discount', 'Total', 'Size', 'CreatedAt', 'UpdatedAt'];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';
}
