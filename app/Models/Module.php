<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

    /**
     * Generated
     */

    protected $table = 'module';
    protected $fillable = ['Id', 'Name', 'Ordering', 'ParentId', 'CreatedAt', 'UpdatedAt'];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';
}
