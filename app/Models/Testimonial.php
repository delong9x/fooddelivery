<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model {

    /**
     * Generated
     */

    protected $table = 'testimonial';
    protected $fillable = ['Id', 'Title', 'Content', 'Name', 'Image', 'Link', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';
}
