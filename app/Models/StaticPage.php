<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model {

    /**
     * Generated
     */

    protected $table = 'static_page';
    protected $fillable = ['Id', 'SeoUrl', 'Title', 'MetaTitle', 'Description', 'MetaDescription', 'Content', 'Image', 'IsActive', 'IsDeleted', 'CreatedById', 'UpdatedById', 'CreatedAt', 'UpdatedAt'];

    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    protected $primaryKey = 'Id';

}
