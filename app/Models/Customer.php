<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

    /**
     * Generated
     */

    protected $table = 'customer';
    protected $fillable = ['Id', 'FirstName', 'LastName', 'Username', 'Password', 'Address', 'City', 'State', 'PostalCode', 'Country', 'Phone', 'Email', 'Avatar', 'CreditCardTypeId', 'BillingAddress', 'BillingCity', 'BillingRegion', 'BillingPostalCode', 'BillingCountry', 'ShipAddress', 'ShipCity', 'ShipRegion', 'ShopPostalCode', 'ShopCountry', 'LoginIp', 'LastLogin', 'Token', 'SocialLogin', 'Point', 'IsActive', 'IsDeleted', 'RefreshToken', 'CreatedAt', 'UpdatedAt', 'TotalBuy'];
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

}
