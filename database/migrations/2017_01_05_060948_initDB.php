<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // config
        Schema::create('config', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Title')->nullable();
            $table->string('Key')->nullable();
            $table->string('Value')->nullable();
            $table->string('Type')->nullable();
            $table->boolean('IsFixed')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('testimonial', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Title')->nullable();
            $table->string('Content')->nullable();
            $table->string('Name')->nullable();
            $table->string('Image')->nullable();
            $table->string('Link')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('social', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('ApiVersion')->nullable();
            $table->string('ClientId')->nullable();
            $table->string('Secret')->nullable();
            $table->integer('Ordering')->nullable();
            $table->string('Image')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('banner', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Image')->nullable();
            $table->string('Alt')->nullable();
            $table->string('Link')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('static_page', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('SeoUrl')->nullable();
            $table->string('Title')->nullable();
            $table->string('MetaTitle')->nullable();
            $table->string('Description')->nullable();
            $table->string('MetaDescription')->nullable();
            $table->text('Content')->nullable();
            $table->string('Image')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        // end config
        // admin
        Schema::create('administrator', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('Username')->nullable();
            $table->string('Password')->nullable();
            $table->string('Address')->nullable();
            $table->string('City')->nullable();
            $table->string('State')->nullable();
            $table->string('PostalCode')->nullable();
            $table->string('Country')->nullable();
            $table->string('RememberToken')->nullable();
            $table->string('Phone')->nullable();
            $table->string('Email')->nullable();
            $table->string('Avatar')->nullable();
            $table->string('GroupId')->nullable();
            $table->string('LoginIp')->nullable();
            $table->string('LastLogin')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('group_administrator', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('Image')->nullable();
            $table->text('Role')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('role', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('module', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('Ordering')->nullable();
            $table->string('ParentId')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        // end admin

        // article

        Schema::create('article', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('SeoUrl')->nullable();
            $table->string('Title')->nullable();
            $table->string('MetaTitle')->nullable();
            $table->string('MetaDescription')->nullable();
            $table->string('Description')->nullable();
            $table->text('Content')->nullable();
            $table->string('Image')->nullable();
            $table->string('YoutubeLink')->nullable();
            $table->string('ArticleRelated')->nullable();
            $table->string('FoodRelated')->nullable();
            $table->boolean('IsVideo')->nullable();
            $table->boolean('IsHomepage')->nullable();
            $table->boolean('IsHot')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->string('CloudTag')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('article_category', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->text('Description')->nullable();
            $table->text('Content')->nullable();
            $table->integer('Ordering')->nullable();
            $table->integer('ParentId')->nullable()->default(0);
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('category_article_relation', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('ArticleId')->nullable();
            $table->integer('CategoryArticleId')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        // order
        Schema::create('order', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('CustomerId')->nullable();
            $table->string('CustomerName')->nullable();
            $table->string('CustomerEmail')->nullable();
            $table->string('CustomerPhone')->nullable();
            $table->string('Ordering')->nullable();
            $table->integer('ShipperId')->nullable();
            $table->timestamp('ShipDate')->nullable();
            $table->timestamp('RequiredDate')->nullable();
            $table->string('Freight')->nullable();
            $table->integer('SaleTax')->nullable();
            $table->integer('Total')->nullable();
            $table->smallInteger('TransactionStatus')->nullable();
            $table->string('ErrorLog')->nullable();
            $table->string('ErrorMessenger')->nullable();
            $table->boolean('Fulfilled')->nullable();
            $table->boolean('IsFail')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->boolean('isPaid')->nullable();
            $table->boolean('IsSell')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });


        Schema::create('order_detail', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('OrderId')->nullable();
            $table->string('FoodId')->nullable();
            $table->string('OrderNumber')->nullable();
            $table->integer('Price')->nullable();
            $table->integer('Quantity')->nullable();
            $table->integer('Discount')->nullable();
            $table->integer('Total')->nullable();
            $table->string('Size')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        // end order

        // employee
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('Email')->nullable();
            $table->string('Phone')->nullable();
            $table->string('Salary')->nullable();
            $table->integer('DepartmentId')->nullable()->default(0);
            $table->string('Rate')->nullable()->default('0');
            $table->timestamp('StartWorkAt')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('Department', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('Email')->nullable();
            $table->string('Phone')->nullable();
            $table->integer('Ordering')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('shipper', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('Email')->nullable();
            $table->string('Phone')->nullable();
            $table->string('Salary')->nullable();
            $table->string('Rate')->nullable()->default('0');
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });


        Schema::create('shipper_time_sheet', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('ShipperId')->nullable();
            $table->integer('OrderId')->nullable();
            $table->timestamp('StartTime')->nullable();
            $table->timestamp('EndTime')->nullable();
            $table->boolean('IsRefuse')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        //end employee

        //customer
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('Username')->nullable()->unique();
            $table->string('Password')->nullable();
            $table->string('Address')->nullable();
            $table->string('City')->nullable();
            $table->string('State')->nullable();
            $table->string('PostalCode')->nullable();
            $table->string('Country')->nullable();
            $table->string('Phone')->nullable();
            $table->string('Email')->nullable();
            $table->string('Avatar')->nullable();
            $table->integer('CreditCardTypeId')->nullable();
            $table->string('BillingAddress')->nullable();
            $table->string('BillingCity')->nullable();
            $table->string('BillingRegion')->nullable();
            $table->string('BillingPostalCode')->nullable();
            $table->string('BillingCountry')->nullable();
            $table->string('ShipAddress')->nullable();
            $table->string('ShipCity')->nullable();
            $table->string('ShipRegion')->nullable();
            $table->string('ShopPostalCode')->nullable();
            $table->string('ShopCountry')->nullable();
            $table->string('LoginIp')->nullable();
            $table->timestamp('LastLogin')->nullable();
            $table->string('Token')->nullable();
            $table->string('SocialLogin')->nullable();
            $table->string('Point')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->string('RefreshToken')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
            $table->integer('TotalBuy')->nulable()->default(0);
        });

        Schema::create('contact', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('Email')->nullable();
            $table->string('Phone')->nullable();
            $table->boolean('Content')->nullable();
            $table->boolean('IsReplied')->nullable();
            $table->boolean('IsEmailDiscountReceiver')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        // food
        Schema::create('food', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('FoodCategoryId')->nullable();
            $table->string('Title')->nullable();
            $table->text('Description')->nullable();
            $table->string('SubTitle')->nullable();
            $table->string('SeoUrl')->nullable();
            $table->string('MetaTitle')->nullable();
            $table->text('MetaDescription')->nullable();
            $table->string('Note')->nullable();
            $table->text('Content')->nullable();
            $table->string('Photo')->nullable();
            $table->text('Image')->nullable();
            $table->integer('Price')->nullable();
            $table->integer('PriceDown')->nullable();
            $table->string('ListColor')->nullable();
            $table->string('ListSize')->nullable();
            $table->text('RelatedFood')->nullable();
            $table->text('RelatedArticle')->nullable();
            $table->integer('BuyNumber')->nullable();
            $table->integer('Ranking')->nullable();
            $table->boolean('DiscountAvailable')->nullable();
            $table->integer('Quantify')->nullable();
            $table->boolean('IsStock')->nullable();
            $table->boolean('IsHomepage')->nullable();
            $table->boolean('IsHot')->nullable();
            $table->string('CloudTag')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('category_food_relation', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('FoodId')->nullable();
            $table->integer('FoodCategoryId')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('food_category', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('SeoUrl')->nullable();
            $table->string('Title')->nullable();
            $table->string('Description')->nullable();
            $table->string('MetaTitle')->nullable();
            $table->text('MetaDescription')->nullable();
            $table->integer('foodNumber')->nullable();
            $table->integer('ParentId')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('collection_food_relation', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('FoodId')->nullable();
            $table->integer('CollectionId')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('food_collection', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('SeoUrl')->nullable();
            $table->string('Title')->nullable();
            $table->string('Description')->nullable();
            $table->string('MetaTitle')->nullable();
            $table->text('MetaDescription')->nullable();
            $table->integer('FoodNumber')->nullable();
            $table->integer('ParentId')->nullable();
            $table->timestamp('StartTime')->nullable();
            $table->timestamp('EndTime')->nullable();
            $table->boolean('IsActive')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });

        Schema::create('comment', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('TargetId')->nullable();
            $table->boolean('Type')->nullable();
            $table->string('Title')->nullable();
            $table->string('Content')->nullable();
            $table->string('Name')->nullable();
            $table->string('Image')->nullable();
            $table->boolean('IsAdmin')->nullable();
            $table->integer('UserID')->nullable();
            $table->boolean('IsDeleted')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        //

        Schema::create('menu', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Name')->nullable();
            $table->string('WeekDay')->nullable();
            $table->char('PositionCode')->nullable();
            $table->smallInteger('Ordering')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
        Schema::create('menu_food_relation', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('MenuId')->nullable();
            $table->integer('FoodId')->nullable();
            $table->smallInteger('Ordering')->nullable();
            $table->integer('CreatedById')->nullable();
            $table->integer('UpdatedById')->nullable();
            $table->timestamp('CreatedAt')->nullable();
            $table->timestamp('UpdatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('config');
        Schema::dropIfExists('banner');

        Schema::dropIfExists('administrator');

        Schema::dropIfExists('article');
        Schema::dropIfExists('article_category');
        Schema::dropIfExists('category_article_relation');


        Schema::dropIfExists('contact');
        Schema::dropIfExists('customer');
        Schema::dropIfExists('group_administrator');
        Schema::dropIfExists('module');
        Schema::dropIfExists('role');

        Schema::dropIfExists('order');
        Schema::dropIfExists('order_detail');
        Schema::dropIfExists('food');
        Schema::dropIfExists('food_category');
        Schema::dropIfExists('category_food_relation');
        Schema::dropIfExists('food_collection');
        Schema::dropIfExists('collection_food_relation');
        Schema::dropIfExists('comment');


        Schema::dropIfExists('Department');
        Schema::dropIfExists('employee');
        Schema::dropIfExists('shipper');
        Schema::dropIfExists('shipper_time_sheet');

        Schema::dropIfExists('social');
        Schema::dropIfExists('static_page');
        Schema::dropIfExists('testimonial');
        Schema::dropIfExists('warehouse');


        Schema::dropIfExists('menu');
        Schema::dropIfExists('menu_food_relation');
    }
}
