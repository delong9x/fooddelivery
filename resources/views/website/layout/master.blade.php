<!DOCTYPE html>
<html lang="en">
    <title>@yield('title')</title>

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Yellowtail%7cCabin:400,500,600,700,400italic,700italic%7cLibre+Baskerville:400italic%7cGreat+Vibes%7cOswald:400,300,700%7cOpen+Sans:600italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css'>

    <!-- META TAGS -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="index, follow" />

    <!-- CSS STYLESHEETS -->
    <link rel="stylesheet" href="{{asset('website/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('website/css/elixir.css')}}" />
    <link href="{{asset('website/js//owl-carousel/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('website/js//owl-carousel/owl.theme.css')}}" rel="stylesheet">
    <link href="{{asset('website/js//owl-carousel/owl.transitions.css')}}" rel="stylesheet">
    <link href="{{asset('website/css/YTPlayer.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('website/css/swipebox.css')}}">

    <!-- Custom CSS -->
    @yield('css')

    <!--[if lt IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <script src="{{asset('website/js//html5shiv.js')}}"></script>
    <script src="{{asset('website/js//respond.js')}}"></script>
    <![endif]-->


</head>
<body>
@include('website.shared._header')

<div id="mask">
    <div class="loader">
        <img src="{{asset('website/svg-loaders/tail-spin.svg')}}" alt='loading'>
    </div>
</div>

<div class="container">
    @yield('content')
</div>
@include('website.shared._footer')

<a href="#0" class="cd-top">Top</a>
<!-- BEGIN Scripts-->

<script src="{{asset('website/js/modernizr.custom.js')}}"></script>
<script src="{{asset('website/js/jquery.js')}}"></script>
<script src="{{asset('website/js/classie.js')}}"></script>
<script src="{{asset('website/js/pathLoader.js')}}"></script>
<script src="{{asset('website/js/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('website/js/jquery.inview.js')}}"></script>
<script src="{{asset('website/js/jquery.nav.js')}}"></script>
<script src="{{asset('website/js/jquery.mb.YTPlayer.js')}}"></script>
<script src="{{asset('website/js/jquery.form.js')}}"></script>
<script src="{{asset('website/js/jquery.validate.js')}}"></script>
<script src="{{asset('website/js/bootstrap.min.js')}}"></script>
<script src="{{asset('website/js/default.js')}}"></script>
<script src="{{asset('website/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('website/js/jquery.isotope.min.js')}}"></script>
<script type="text/javascript" src="{{asset('website/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('website/js/jquery.swipebox.js')}}"></script>



@yield('js')


<!-- END Scripts -->


</body>
</html>