@extends('website.layout.master')
@section('title', 'Menu')
@section('css')
    <style>
        section.menu {
            background: none;
        }

        #header {
            position: fixed;
            top: 0px;
            left: 0px;
            z-index: 1001;
            width: 100%;
            height: 90px;
            background-color: #fff !important;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.11);
            padding-top: 20px;
        }

        #header .menu-item a {
            color: #818181;
        }

        #header.overflow {
            top: -100px;
            left: 0px;
            background-color: #fff !important;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.11);
            -webkit-transition: all 0.3s ease-in;
            -o-transition: all 0.3s ease-in;
            transition: all 0.3s ease-in;
            -webkit-font-smoothing: antialiased;
        }
    </style>


@endsection



@section('content')

    <!-- BEGIN MENU SECTION -->
    <section id="menu" class="section menu">

        <div class="container">
            <div class="jt_row jt_row-fluid row">

                <div class="col-md-12 jt_col column_container">
                    <div class="voffset100"></div>
                    <div class="title first">Sushi</div>
                    <div class="voffset10"></div>
                    <div class="subtitle">Exclusive and delicious sushi</div>
                    <img class="center" src="{{asset('website/images/sushi.jpg')}}" alt="Sushi">

                </div>

                <div class="col-md-4 jt_col column_container">
                    <div class="voffset10"></div>
                    <ul class="menu">
                        <li>
                            Maguro Nigiri
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Kappa Maki
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Sake Nigiri
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Ikura Gukan
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Amaebi
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 jt_col column_container">
                    <div class="voffset10"></div>
                    <ul class="menu">
                        <li>
                            Maguro Nigiri
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Kappa Maki
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Sake Nigiri
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Ikura Gukan
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Amaebi
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 jt_col column_container">
                    <div class="voffset10"></div>
                    <ul class="menu">
                        <li>
                            Maguro Nigiri
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Kappa Maki
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Sake Nigiri
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Ikura Gukan
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Amaebi
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <div class="title">Soups</div>
                    <div class="voffset10"></div>
                    <div class="subtitle">Exclusive and delicious soups</div>
                    <img class="center" src="{{asset('website/images/soups.jpg')}}" alt="Soups">

                </div>

                <div class="col-md-4 jt_col column_container">
                    <div class="voffset10"></div>
                    <ul class="menu">
                        <li>
                            Maguro Nigiri
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Kappa Maki
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Sake Nigiri
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Ikura Gukan
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Amaebi
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 jt_col column_container">
                    <div class="voffset10"></div>
                    <ul class="menu">
                        <li>
                            Maguro Nigiri
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Kappa Maki
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Sake Nigiri
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Ikura Gukan
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Amaebi
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 jt_col column_container">
                    <div class="voffset10"></div>
                    <ul class="menu">
                        <li>
                            Maguro Nigiri
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Kappa Maki
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Sake Nigiri
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Ikura Gukan
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Amaebi
                            <div class="detail">10 oz Greg Norman Ranch, Australia<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <div class="title">Salads</div>
                    <div class="voffset10"></div>
                    <div class="subtitle">Exclusive and delicious salads</div>
                    <img class="center" src="{{asset('website/images/salads.jpg')}}" alt="Salads">

                </div>

                <div class="col-md-4 jt_col column_container">
                    <div class="voffset30"></div>
                    <ul class="menu">
                        <li>
                            Italian Salad
                            <div class="detail">Mozarella, Pomodoro & Parmagiano<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Jabugo Ham
                            <div class="detail">Jabugo ham, eggs, cheese & Red Pepper<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Cesar
                            <div class="detail">Cesar sauce, Toasts, cheese & Chicken<span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            BBQ Salad
                            <div class="detail">Onion, Bacon, BBQ sauce & Cheese<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Swiss Salad
                            <div class="detail">Diferent swiss cheeses and Secret sauce<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 jt_col column_container">
                    <div class="voffset30"></div>
                    <ul class="menu">
                        <li>
                            Italian Salad
                            <div class="detail">Mozarella, Pomodoro & Parmagiano<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Jabugo Ham
                            <div class="detail">Jabugo ham, eggs, cheese & Red Pepper<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Cesar
                            <div class="detail">Cesar sauce, Toasts, cheese & Chicken<span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            BBQ Salad
                            <div class="detail">Onion, Bacon, BBQ sauce & Cheese<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Swiss Salad
                            <div class="detail">Diferent swiss cheeses and Secret sauce<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 jt_col column_container">
                    <div class="voffset30"></div>
                    <ul class="menu">
                        <li>
                            Italian Salad
                            <div class="detail">Mozarella, Pomodoro & Parmagiano<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Jabugo Ham
                            <div class="detail">Jabugo ham, eggs, cheese & Red Pepper<span class="price">$20.50</span>
                            </div>
                        </li>
                        <li>
                            Cesar
                            <div class="detail">Cesar sauce, Toasts, cheese & Chicken<span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            BBQ Salad
                            <div class="detail">Onion, Bacon, BBQ sauce & Cheese<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Swiss Salad
                            <div class="detail">Diferent swiss cheeses and Secret sauce<span class="price">$17.99</span>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 col-md-offset-4 jt_col column_container">
                    <div class="voffset60"></div>
                    <div class="ornament"></div>
                </div>
            </div>
        </div>
        <div class="voffset60"></div>
    </section>
    <!-- END MENU SECTION -->







    <!-- BEGIN CONTACT SECTION -->
    <section id="contact" class="section contact dark">
        <div class="container">
            <div class="jt_row jt_row-fluid row">

                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">Contact</h2>
                </div>
                <div class="col-md-6 col-md-offset-3 jt_col column_container">
                    <div class="section-subtitle">
                        W325 State Road 123 Mondovi, WI (Wisconsin) 98746-54321
                    </div>
                </div>
                <form action="mail.php" method="post" id="contactform" class="contact-form">
                    <div class="col-md-6 jt_col column_container">
                        <input type="text" id="name" name="name" class="text name required" placeholder="Name">
                        <input type="email" id="email" name="email" class="tex email required" placeholder="Email">
                        <input type="text" id="subject" name="subject" placeholder="Subject">
                    </div>

                    <div class="col-md-6 jt_col column_container">
                        <textarea id="message" name="message" class="text area required" placeholder="Message"
                                  rows="10"></textarea>
                    </div>

                    <div class="col-md-4 col-md-offset-4 jt_col column_container">
                        <div class="formSent"><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.
                        </div>
                        <input type="submit" class="button contact center" value="Submit">
                    </div>

                </form>
                <div class="voffset100"></div>
            </div>
            <div class="voffset50"></div>
        </div>

    </section>
    <!-- END CONTACT SECTION -->





    <!-- BEGIN MAP SECTION -->
    <section id="maps">
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <div class="map-content">
            <div class="wpgmappity_container inner-map" id="wpgmappitymap"></div>
        </div>
    </section>
    <!-- END MAP SECTION -->



@endsection

@section('js')

    <script type="text/javascript">
        ;
        (function ($) {

            $('.swipebox').swipebox();

        })(jQuery);
    </script>
@endsection

    

