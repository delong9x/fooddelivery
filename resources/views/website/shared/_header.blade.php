
<!-- BEGIN HEADER -->
<header id="header" role="banner">
    <div class="jt_row container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand normal logo" href="#home"><div class="logo_elixir"></div></a>
            <a class="navbar-brand mini" href="#home"><div class="logo_elixir dark"></div></a>
            <a class="navbar-brand mini darker" href="#home"><div class="logo_elixir dark"></div></a>
        </div>

        <!-- BEGIN NAVIGATION MENU-->
        <nav class="collapse navbar-collapse navbar-right navbar-main-collapse" role="navigation">
            <ul id="nav" class="nav navbar-nav navigation">
                <li class="page-scroll menu-item"><a href="{{url('/')}}">Home</a></li>
                <li class="page-scroll menu-item"><a href="{{url('/#about')}}">About</a></li>
                <li class="page-scroll menu-item"><a href="{{url('/menu')}}">Menu</a></li>
                <li class="page-scroll menu-item"><a href="{{url('/#gallery')}}">Gallery</a></li>
                <li class="page-scroll menu-item"><a href="{{url('/#reservations')}}">Reservations</a></li>
                <li class="page-scroll menu-item"><a href="{{url('/#contact')}}">Location</a></li>
            </ul>
        </nav>
        <!-- EN NAVIGATION MENU -->
    </div>
</header>
<!-- END HEADER -->