@extends('website.layout.master')
@section('title', 'Food Delivery')

@section('content')


    <!-- BEGIN HOME SLIDER SECTION -->
    <section id="home-slider">
        <div class="overlay"></div>
        <!-- SLIDER IMAGES -->
        <div id="owl-main" class="owl-carousel">
            <div class="item"><img src="<?=asset('website/images/slider/01.jpg')?>" alt="Slide 01"></div>
            <div class="item"><img src="<?=asset('website/images/slider/02.jpg')?>" alt="Slide 02"></div>
            <div class="item"><img src="<?=asset('website/images/slider/03.jpg')?>" alt="Slide 03"></div>
        </div>
        <!-- SLIDER CONTENT -->
        <div class="slide-content">
            <div class="voffset100"></div>
            <span class="logointro"></span>
            <div id="owl-main-text" class="owl-carousel">
                <div class="item">
                    <h2>Japanese Style Restaurant</h2>
                </div>
                <div class="item">
                    <h2>Welcome to restaurant</h2>
                </div>
                <div class="item">
                    <h2>Elixir exclusively food</h2>
                </div>
            </div>
            <div class="slide-sep"></div>
            <p>The history of sushi in Japan began around the 8th century</p>

        </div>
        <!-- BOTTOM ANIMATED ARROW -->
        <a href="#about" class="page-scroll menu-item">
            <div class="mouse"><span></span></div>
        </a>
    </section>
    <!-- END HOME SLIDER SECTION -->


    <!-- BEGIN ABOUT SECTION -->
    <section id="about" class="section about">
        <div class="container">
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">About Us</h2>
                </div>
                <div class="col-md-6 col-md-offset-3 jt_col column_container">
                    <div class="section-subtitle">
                        We love restaurants as much as you do. That’s why we’ve been helping them fill tables since
                        1999.
                        Welcome to elixir restaurant
                    </div>
                </div>


                <div class="col-md-6 jt_col column_container">
                    <h2 class="heading font-smoothing">The History</h2>
                    <p class="text">
                        The <strong>History of Kitchens</strong> and Cooks gives further intimation on Mr Boulanger
                        usual menu, stating confidently that "Boulanger served salted poultry and
                        fresh eggs, all presented without a tablecloth on small marble tables".
                        Numerous commentators have also referred to the supposed restaurant
                        owner's eccentric habit of touting for custom outside his establishment,
                        dressed in aristocratic fashion and brandishing a sword
                    </p>
                    <p class="text">
                        According to Miss Spang, there is not a shred of evidence for any of it.
                        She said: These legends just get passed on by hearsay and then spiral
                        out of control. Her interest in <strong>Boulanger</strong> dates back to a history of
                        food seminar in Paris in the mid-1990s
                    </p>

                    <div class="ornament"></div>
                </div>

                <div class="col-md-6 jt_col column_container">
                    <div class="voffset40"></div>
                    <div id="owl-about" class="owl-carousel">
                        <div class="item"><img src="<?=asset('website/images/about01.jpg')?>" alt=""></div>
                        <div class="item"><img src="<?=asset('website/images/about02.jpg')?>" alt=""></div>
                        <div class="item"><img src="<?=asset('website/images/about03.jpg')?>" alt=""></div>
                    </div>
                </div>
            </div>
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-6 jt_col column_container">
                    <div class="voffset10"></div>
                    <div id="owl-about2" class="owl-carousel">
                        <div class="item"><img src="<?=asset('website/images/about04.jpg')?>" alt=""></div>
                        <div class="item"><img src="<?=asset('website/images/about05.jpg')?>" alt=""></div>
                        <div class="item"><img src="<?=asset('website/images/about06.jpg')?>" alt=""></div>
                    </div>
                </div>


                <div class="col-md-6 jt_col column_container">

                    <p class="text">
                        The <strong>History of Kitchens</strong> and Cooks gives further intimation on Mr Boulanger
                        usual menu, stating confidently that "Boulanger served salted poultry and
                        fresh eggs, all presented without a tablecloth on small marble tables".
                        Numerous commentators have also referred to the supposed restaurant
                        owner's eccentric habit of touting for custom outside his establishment,
                        dressed in aristocratic fashion and brandishing a sword
                    </p>
                    <p class="text">
                        According to Miss Spang, there is not a shred of evidence for any of it.
                        She said: These legends just get passed on by hearsay and then spiral
                        out of control. Her interest in <strong>Boulanger</strong> dates back to a history of
                        food seminar in Paris in the mid-1990s
                    </p>
                </div>
            </div>

        </div>

    </section>
    <!-- END ABOUT SECTION -->


    <!-- BEGIN TIMETABLE SECTION -->
    <section id="timetable" class="section timetable parallax">
        <div class="container">
            <div class="jt_row jt_row-fluid row">
                <span class="column-divider"></span>
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title"><span class="timetable-decorator"></span><span class="opening-hours">Opening Hours</span><span
                                class="timetable-decorator2"></span></h2>
                </div>
                <div class="col-md-12 jt_col column_container">
                    <div class="section-subtitle">
                        Call For Reservations
                    </div>
                </div>

                <div class="col-md-3 col-md-offset-3 jt_col column_container">
                    <div class="section-subtitle days">
                        Sunday to Tuesday
                    </div>
                    <div class="section-subtitle hours">
                        09:00<br>24:00
                    </div>
                </div>


                <div class="col-md-3 jt_col column_container">
                    <div class="section-subtitle days">
                        Friday and Saturday
                    </div>
                    <div class="section-subtitle hours">
                        08:00<br>03:00
                    </div>
                </div>
                <div class="col-md-12 jt_col column_container">
                    <div class="number">
                        RESERVATION NUMBER: 0842-5484214
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- END TIMETABLE SECTION -->


    <!-- BEGIN MENU SECTION -->
    <section id="menu" class="section menu">

        <div class="container">
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <div class="voffset100"></div>
                    <img class="center menu-logo" src="<?=asset('website/images/menu_logo.png')?>" alt="Menu Logo">
                    <div class="voffset60"></div>
                </div>

                <div class="col-md-4 jt_col column_container">
                    <h3>Appetizers</h3>
                    <ul>
                        <li>
                            Filet
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Special Filet
                            <div class="detail">10 oz Greg Norman Ranch<span class="price">$20.50</span></div>
                        </li>
                        <li>
                            New York Strip
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Porterhouse
                            <div class="detail">7 oz. Center Cut10 oz.<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Delmonico
                            <div class="detail">10 oz Greg Norman Ranch<span class="price">$17.99</span></div>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 jt_col column_container">
                    <h3>Our Sushi</h3>
                    <ul>
                        <li>
                            Filet
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Special Filet
                            <div class="detail">10 oz Greg Norman Ranch<span class="price">$20.50</span></div>
                        </li>
                        <li>
                            New York Strip
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Porterhouse
                            <div class="detail">7 oz. Center Cut10 oz.<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Delmonico
                            <div class="detail">10 oz Greg Norman Ranch<span class="price">$17.99</span></div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 jt_col column_container">
                    <h3>Drinks</h3>
                    <ul>
                        <li>
                            Filet
                            <div class="detail">7 oz. Center Cut10 oz. Double cut<span class="price">$14.49</span></div>
                        </li>
                        <li>
                            Special Filet
                            <div class="detail">10 oz Greg Norman Ranch<span class="price">$20.50</span></div>
                        </li>
                        <li>
                            New York Strip
                            <div class="detail">9 oz. Center Cut12 oz. Double cut oz <span class="price">$9.99</span>
                            </div>
                        </li>
                        <li>
                            Porterhouse
                            <div class="detail">7 oz. Center Cut10 oz.<span class="price">$7.99</span></div>
                        </li>
                        <li>
                            Delmonico
                            <div class="detail">10 oz Greg Norman Ranch<span class="price">$17.99</span></div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-md-offset-4 jt_col column_container">
                    <a href="menu.html" class="button menu center">View Complete Menu</a>
                </div>

            </div>
        </div>
        <div class="voffset30"></div>
    </section>
    <!-- END MENU SECTION -->


    <!-- BEGIN GALLERY SECTION -->
    <section id="gallery" class="section gallery dark">
        <div class="jt_row jt_row-fluid row">
            <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">Gallery</h2>
                <div class="col-md-6 col-md-offset-3 jt_col column_container">
                    <div class="section-subtitle">
                        If a picture says a thousand words, then you can imagine how long it would take to describe all
                        our
                        mouthwatering selections.
                    </div>
                </div>

            </div>

            <div class="col-md-12 jt_col column_container">

                <nav class="primary">
                    <ul>
                        <li><a href="#" data-filter="*" class="selected"><span>All Photos</span></a></li>
                        <li><a href="#" data-filter=".sushi"><span>Sushi</span></a></li>
                        <li><a href="#" data-filter=".soup"><span>Soups</span></a></li>
                        <li><a href="#" data-filter=".salad"><span>Salads</span></a></li>
                    </ul>
                </nav>

                <div class="portfolio">

                    <article class="entry sushi">
                        <a class="swipebox" href="<?=asset('website/images/gallery/01.jpg')?>">
                            <img src="<?=asset('website/images/gallery/01.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry sushi">
                        <a class="swipebox" href="<?=asset('website/images/gallery/02.jpg')?>">
                            <img src="<?=asset('website/images/gallery/02.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry sushi">
                        <a class="swipebox" href="<?=asset('website/images/gallery/03.jpg')?>">
                            <img src="<?=asset('website/images/gallery/03.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry sushi">
                        <a class="swipebox" href="<?=asset('website/images/gallery/04.jpg')?>">
                            <img src="<?=asset('website/images/gallery/04.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry soup">
                        <a class="swipebox" href="<?=asset('website/images/gallery/05.jpg')?>">
                            <img src="<?=asset('website/images/gallery/05.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry salad">
                        <a class="swipebox" href="<?=asset('website/images/gallery/06.jpg')?>">
                            <img src="<?=asset('website/images/gallery/06.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry sushi">
                        <a class="swipebox" href="<?=asset('website/images/gallery/07.jpg')?>">
                            <img src="<?=asset('website/images/gallery/07.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry salad">
                        <a class="swipebox" href="<?=asset('website/images/gallery/08.jpg')?>">
                            <img src="<?=asset('website/images/gallery/08.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>

                    <article class="entry soup">
                        <a class="swipebox" href="<?=asset('website/images/gallery/09.jpg')?>">
                            <img src="<?=asset('website/images/gallery/09.jpg')?>" class="img-responsive" alt=""/>
                            <span class="magnifier"></span>
                        </a>
                    </article>


                </div>
            </div> <!-- End .jt_col -->
        </div> <!-- End .jt_row -->
        <div class="voffset200"></div>
    </section>
    <!-- END GALLERY SECTION -->


    <!-- BEGIN RESERVATIONS SECTION -->
    <section id="reservations" class="section reservations">
        <div class="container">
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">Reservations</h2>
                </div>
                <div class="col-md-6 col-md-offset-3 jt_col column_container">
                    <div class="section-subtitle">
                        Booking a table has never been so easy with free & instant online restaurant reservations,
                        booking
                        now!!
                    </div>
                </div>
                <div class="col-md-12 jt_col column_container">
                    <div class="reservations_logo"></div>
                    <div class="voffset30"></div>

                    <h4><span class="above">Welcome to Elixir</span></h4>
                    <h3>Make a Reservation</h3>
                    <h4 class="below"><span>Open Hours</span></h4>
                    <p><strong>Sunday to Tuesday</strong> 09.00 - 24:00 <strong>Friday and Sunday</strong> 08:00 - 03.00
                    </p>
                    <h3 class="reservation-phone">+1-506-890-0179</h3>
                </div>

                <form action="reservations.php" method="post" id="reservationform" class="contact-form">
                    <div class="col-md-5 col-md-offset-1 jt_col column_container">
                        <p>Book a table</p>
                        <input type="text" id="date" name="date" placeholder="Date" class="text date required">
                        <input type="text" id="time" name="time" placeholder="Time" class="text time required">
                        <input type="text" id="party" name="party" placeholder="Party" class="text party required">
                    </div>

                    <div class="col-md-5 jt_col column_container">
                        <p>Contact Details</p>
                        <input type="text" id="reservation_name" name="reservation_name" placeholder="Name"
                               class="text reservation_name required">
                        <input type="email" id="reservation_email" name="reservation_email" class="tex email required"
                               placeholder="Email">
                        <input type="text" id="reservation_phone" name="reservation_phone" placeholder="Phone"
                               class="text reservation_phone required">
                    </div>

                    <div class="col-md-10 col-md-offset-1 jt_col column_container">
                    <textarea id="reservation_message" name="reservation_message" class="text area required"
                              placeholder="Message" rows="6"></textarea>
                    </div>
                    <div class="col-md-4 col-md-offset-4 jt_col column_container">
                        <div class="formSent"><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.
                        </div>
                        <input type="submit" class="button center" value="Make reservation">
                    </div>


                </form>
                <div class="col-md-12 jt_col column_container">
                    <div class="voffset60"></div>
                    <div class="ornament"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- END RESERVATIONS SECTION-->


    <!-- BEGIN TESTIMONIALS SECTION -->
    <section id="testimonials" class="section testimonials parallax voffset100 clearfix">

        <div class="container">
            <div class="jt_row jt_row-fluid row">
                <div class="voffset20"></div>
                <div class="col-md-12 jt_col column_container">
                    <div class="testimonials_img"></div>
                    <h2>Testimonials<span>”</span></h2>
                </div>


                <div class="col-md-12 jt_col column_container">
                    <div class="testimonials carousel-wrapper with_pagination">
                        <div class="owl-carousel generic-carousel">
                            <div class="item">
                                <p>Applicake chocolate cake wafer toffee pie soufflé wafer. Tart marshmallow wafer
                                    macaroon
                                    cheesecake jelly. Gingerbread cookie soufflé sweet roll sweet roll jelly-o.</p>
                                <span class="author">Alexander Smith</span>
                            </div>
                            <div class="item">
                                <p>"Awesome to work with. Incredibly organized, easy to communicate with, responsive
                                    with
                                    next iterations, and beautiful work. It’s great to work with someone so open-minded
                                    and
                                    responsive. Thank you!"</p>
                                <span class="author">John Berthier</span>
                            </div>
                            <div class="item">
                                <p>“Your designs were exactly what Josef had always imagined — clear, clean, continuous,
                                    with a focus on stylistic elements. It was a major help for us. Thank you so much
                                    for
                                    your work on this project."</p>
                                <span class="author">Carolyn Meyer</span>
                            </div>
                        </div>
                    </div>
                    <div class="voffset80"></div>
                </div>
            </div>
        </div> <!-- End .container -->
    </section>
    <!-- END TESTIMONIALS SECTION -->


    <!-- BEGIN CONTACT SECTION -->
    <section id="contact" class="section contact dark">
        <div class="container">
            <div class="jt_row jt_row-fluid row">

                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">Contact</h2>
                </div>
                <div class="col-md-6 col-md-offset-3 jt_col column_container">
                    <div class="section-subtitle">
                        W325 State Road 123 Mondovi, WI (Wisconsin) 98746-54321
                    </div>
                </div>
                <form action="mail.php" method="post" id="contactform" class="contact-form">
                    <div class="col-md-6 jt_col column_container">
                        <input type="text" id="name" name="name" class="text name required" placeholder="Name">
                        <input type="email" id="email" name="email" class="tex email required" placeholder="Email">
                        <input type="text" id="subject" name="subject" placeholder="Subject">
                    </div>

                    <div class="col-md-6 jt_col column_container">
                    <textarea id="message" name="message" class="text area required" placeholder="Message"
                              rows="10"></textarea>
                    </div>

                    <div class="col-md-4 col-md-offset-4 jt_col column_container">
                        <div class="formSent"><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.
                        </div>
                        <input type="submit" class="button contact center" value="Submit">
                    </div>

                </form>
                <div class="voffset100"></div>
            </div>
            <div class="voffset50"></div>
        </div>

    </section>
    <!-- END CONTACT SECTION -->


    <!-- BEGIN MAP SECTION -->
    <section id="maps">
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <div class="map-content">
            <div class="wpgmappity_container inner-map" id="wpgmappitymap"></div>
        </div>
    </section>
    <!-- END MAP SECTION -->

@endsection


@section('js')

    <script type="text/javascript">
        ;( function( $ ) {

            $( '.swipebox' ).swipebox();

        } )( jQuery );
    </script>
@endsection