<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->

    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{asset('admin/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/animate.min.css')}}" rel="stylesheet">


    <!-- Custom styling plus plugins -->
    <link href="{{asset('admin/css/custom.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/maps/jquery-jvectormap-2.0.3.css')}}"/>
    <link href="{{asset('admin/css/icheck/flat/green.css')}}" rel="stylesheet"/>
    <link href="{{asset('admin/css/floatexamples.css')}}" rel="stylesheet" type="text/css')}}"/>

    <!-- editor -->
    <link href="{{asset('admin/css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/editor/index.css')}}" rel="stylesheet">

    <script src="{{asset('admin/js/jquery.min.js')}}"></script>
    <script src="{{asset('admin/js/nprogress.js')}}"></script>

    <!--[if lt IE 9]>
    <script src="{{asset('admin//js/ie8-responsive-file-warning.js')}}"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
    <![endif]-->

    {{-- custom css --}}
    @yield('css')

</head>
<body class="nav-md">
<div class="container body">


    <div class="main_container">
        @include('admin.shared._header')
        @include('admin.shared._sidebar')
        <!-- page content -->
            <div class="right_col" role="main">
        @yield('content')
        </div>
        @include('admin.shared._footer')
    </div>
</div>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>

<!-- gauge js -->
<script type="text/javascript" src="{{asset('admin/js/gauge/gauge.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/gauge/gauge_demo.js')}}"></script>
<!-- bootstrap progress js -->
<script src="{{asset('admin/js/progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{asset('admin/js/nicescroll/jquery.nicescroll.min.js')}}"></script>
<!-- icheck -->
<script src="{{asset('admin/js/icheck/icheck.min.js')}}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{asset('admin/js/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/datepicker/daterangepicker.js')}}"></script>
<!-- chart js -->
<script src="{{asset('admin/js/chartjs/chart.min.js')}}"></script>

<script src="{{asset('admin/js/custom.js')}}"></script>

<!-- flot js -->
<!--[if lte IE 8]>
<script type="text/javascript" src="{{asset('admin/js/excanvas.min.js')}}"></script><![endif]-->
<script type="text/javascript" src="{{asset('admin/js/flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/jquery.flot.pie.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/jquery.flot.orderBars.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/jquery.flot.time.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/date.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/jquery.flot.spline.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/jquery.flot.stack.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/flot/curvedLines.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/cropping/cropper.min.js')}}"></script>
<!-- worldmap -->
<script type="text/javascript" src="{{asset('admin/js/maps/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/maps/gdp-data.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/maps/jquery-jvectormap-us-aea-en.js')}}"></script>

<!-- richtext editor -->
<script src="{{asset('admin/js/editor/tinymce.min.js')}}"></script>

<!-- pace -->
<script src="{{asset('admin/js/pace/pace.min.js')}}"></script>

<!-- skycons -->
<script src="{{asset('admin/js/skycons/skycons.min.js')}}"></script>

<script src="{{asset('admin/js/script-dev.js')}}"></script>


@yield('js')
</body>

</html>
