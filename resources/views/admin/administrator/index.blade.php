@extends('admin.layout.admin_layout')
@section('title', 'Administrator Manager')

@section('content')


    <!-- top tiles -->
    <div class="row tile_count col-md-12 col-sm-12 col-xs-12">
        <h2>List Administrator</h2>
        <a class="btn btn-primary" href="{{route('CreateAdmin')}}">Add an Administrator</a>
    </div>
    <!-- /top tiles -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">@include('admin.shared._alert')</div>
                <div class="x_content">
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            <h3>Search:</h3>
                            {{ Form::open(array('url' => route('ListAdmin'), 'method'=>'GET')) }}
                            <div class="form-group row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <label class="col-md-12 col-sm-12 col-xs-12">First Name</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="FirstName" class="form-control" value="{{$SearchForm->FirstName}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <label class="col-md-12 col-sm-12 col-xs-12">Email</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Email" class="form-control" value="{{$SearchForm->Email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <label class="col-md-12 col-sm-12 col-xs-12">Last Name</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="LastName" class="form-control" value="{{$SearchForm->LastName}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <label class="col-md-12 col-sm-12 col-xs-12">Phone</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="Phone" class="form-control" value="{{$SearchForm->Phone}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <label class="col-md-12 col-sm-12 col-xs-12">Status</label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <select class="form-control" name="IsActive">
                                            <option @if($SearchForm->IsActive == '0') selected @endif value="0">All</option>
                                            <option @if($SearchForm->IsActive == 'true') selected @endif value="true">True</option>
                                            <option @if($SearchForm->IsActive == 'false') selected @endif value="false">False</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <input
                                        type="submit" value="Search"
                                        class="btn btn-success">
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ListAdmin as $row)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$row->FirstName}} {{$row->LastName}}</td>
                                        <td>{{$row->Username}}</td>
                                        <td>{{$row->Email}}</td>
                                        <td>{{$row->Phone}}</td>
                                        <td>{{$row->CreatedAt->format('d/m/Y')}}</td>
                                        <td>@if($row->IsActive == true) <span
                                                    class="btn btn-success">Active</span> @else <span
                                                    class="btn btn-danger">Not Active</span>  @endif</td>
                                        <td>
                                            <a class="btn btn-info" href="{{route('UpdateAdmin',['id'=>$row->Id])}}">Update</a>
                                            <a class="btn btn-warning btn-delete" href="{{route('DeleteAdmin',['id'=>$row->Id])}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $ListAdmin->links() }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection