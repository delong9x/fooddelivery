@extends('admin.layout.admin_layout')
@section('title', 'Administrator Manager')

@section('css')

@endsection

@section('content')


    <div class="row tile_count">
        <h2>Create A New Administrator</h2>
    </div>
    <!-- /top tiles -->

    @include('admin.shared._alert')

    <div class="row">
        {{ Form::open(array('url' => route('CreateAdmin'), 'method'=>'POST', 'enctype'=>'multipart/form-data')) }}
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Account</h3>
                </div>
                <div class="x_content">
                    <div class="form-group @if(count($errors->get('Username')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">User name*</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" type="text" name="Username" value="{{$data->Username}}"
                                   data-validate-length-range="6" data-validate-words="2" required>
                        </div>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group row @if(count($errors->get('Password')) > 0) has-error @endif">
                        <label class="col-md-12 col-sm-12 col-xs-12">Password*</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" type="password" name="Password" value="" required>
                        </div>

                    </div>
                    <div class="form-group row @if(count($errors->get('Password_confirmation')) > 0) has-error @endif">
                        <label class="col-md-12 col-sm-12 col-xs-12">Confirm Password*</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" type="password" name="Password_confirmation" value="" required>
                        </div>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12" >Active: <input @if($data->IsActive == 1) checked @endif type="radio" name="IsActive" value="1"></label>
                        <label class="col-md-12 col-sm-12 col-xs-12" >Not Active: <input @if($data->IsActive == 0) checked @endif type="radio" name="IsActive" value="0"></label>
                    </div>
                </div>


            </div>

        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>User Information</h3>
                </div>
                <div class="x_content">
                    <div class="form-group row">
                        <label class="col-md-6 col-sm-6 col-xs-12">First Name*</label>
                        <label class="col-md-6 col-sm-6 col-xs-12">Last Name*</label>
                    </div>
                    <div class="form-group row @if(count($errors->get('FirstName')) > 0 || count($errors->get('LastName')) > 0) has-error @endif">

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" type="text" name="FirstName" value="{{$data->FirstName}}"
                                   required>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <input class="form-control" type="text" name="LastName" value="{{$data->LastName}}"
                                   required>
                        </div>

                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group row @if(count($errors->get('Email')) > 0) has-error @endif">
                        <label class="col-md-12 col-sm-12 col-xs-12">Email:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" type="text" name="Email" value="{{$data->Email}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Phone:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" type="text" name="Phone" value="{{$data->Phone}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Address:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" type="text" name="Address" value="{{$data->Address}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Avatar:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="avatar-upload">
                                <input class="avatar-src" name="avatar_src" type="hidden" value="{{$data->Avatar}}">
                                <input class="avatar-data" name="avatar_data" type="hidden"
                                       value="{{$data->avatar_data}}">
                                <label for="avatarInput">Upload:</label>
                                <input type="file" name="AvatarInput" id="avatarInput" class="avatar-upload">
                                <div class="avatar-preview preview-lg">
                                    <img src="" id="preview-avatar">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 row">
            <input class="btn btn-success" type="submit" name="submit" value="Save">
            <input class="btn btn-danger" type="reset" value="Reset">
            <a class="btn btn-info" href="{{route('ListAdmin')}}">Return</a>
        </div>
        {{ Form::close() }}
    </div>

@endsection

@section('js')
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-avatar').attr('src', e.target.result);
                    $('#preview-avatar').show();
                    $('.avatar-data').val(e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#avatarInput").change(function () {
            readURL(this);
        });

        $(document).ready(function () {
            if ($('.avatar-data').val() != "") {
                $("#preview-avatar").attr('src', $('.avatar-data').val())
            } else {
                $("#preview-avatar").hide();
            }
        })
    </script>
@endsection