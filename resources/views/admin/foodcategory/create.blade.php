@extends('admin.layout.admin_layout')
@section('title', 'Create New Food Category')

@section('css')

@endsection

@section('content')


    <div class="row tile_count">
        <h2>Create A New Food Category</h2>
    </div>
    <!-- /top tiles -->

    @include('admin.shared._alert')

    <div class="row">
        {{ Form::open(array('url' => route('CreateFoodCategory'), 'method'=>'POST', 'enctype'=>'multipart/form-data')) }}
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Account</h3>
                </div>
                <div class="x_content">
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Base</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select name="ParentId" class="form-control">
                                <option value="0">---Select---</option>
                                {!! $treeCategory !!}
                            </select>
                        </div>

                    </div>
                    <div class="form-group @if(count($errors->get('Title')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Title*</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="form-control title" type="text" name="Title" value="{{$data->Title}}" required>
                        </div>
                        <label class="col-md-12 col-sm-12 col-xs-12">Slug</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input type="hidden" class="seo_url_generated" name="SeoUrl" value="{{$data->SeoUrl}}">
                            <input class="form-control seo_url_generated" type="text" disabled
                                   value="{{$data->SeoUrl}}">
                        </div>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group @if(count($errors->get('Description')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Description:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <textarea rows="3" class="form-control" type="text" name="Description">{{$data->Description}}</textarea>
                        </div>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12">Active: <input @if($data->IsActive == 1) checked
                                                                                    @endif type="radio" name="IsActive"
                                                                                    value="1"></label>
                        <label class="col-md-12 col-sm-12 col-xs-12">Not Active: <input
                                    @if($data->IsActive == 0) checked @endif type="radio" name="IsActive"
                                    value="0"></label>
                    </div>
                </div>


            </div>

        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>SEO</h3>
                </div>
                <div class="x_content">
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Meta Title</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="form-control metatitle" type="text" name="MetaTitle" value="{{$data->MetaTitle}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Meta Description*</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <textarea class="form-control" type="text" name="MetaDescription" maxlength="160">{{$data->MetaDescription}}</textarea>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <input class="btn btn-success" type="submit" name="submit" value="Save">
            <input class="btn btn-danger" type="reset" value="Reset">
            <a class="btn btn-info" href="{{route('ListFoodCategory')}}">Return</a>
        </div>
        {{ Form::close() }}
    </div>

@endsection

@section('js')
    <script>


    </script>
@endsection