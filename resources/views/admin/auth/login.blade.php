@extends('admin.layout.unauth_layout')

@section('content')
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">


                    {{ Form::open(array('url' => route('DoAdminLogin'), 'method'=>'POST', 'enctype'=>'multipart/form-data')) }}
                        <h1>Agilsun</h1>
                    @include('admin.shared._alert')
                        <div>
                            <input type="text" class="form-control" name="Username" placeholder="Username" required="" value="{{$data->Username}}" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="Password" placeholder="Password" required="" />
                        </div>
                        <div class="form-group text-left">
                            <label class="checkbox-inline"> <input  @if($data->IsRemember == 1) checked @endif type='checkbox' placeholder="IsRemember" value="1"  style="width: 20px"/> Remember </label>
                        </div>
                        <div class="text-center row">
                            <input type="submit" class="btn btn-default submit" value="Log in">
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <div class="clearfix"></div>
                            <br />
                            <div>

                                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                            </div>
                        </div>
                    {{Form::close()}}
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>
@endsection