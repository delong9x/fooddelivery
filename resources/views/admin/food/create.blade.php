@extends('admin.layout.admin_layout')
@section('title', 'Create New Food Category')

@section('css')

@endsection

@section('content')


    <div class="row tile_count">
        <h2>Create A New Food</h2>
    </div>
    <!-- /top tiles -->

    @include('admin.shared._alert')

    <div class="row">
        {{ Form::open(array('url' => route('DoCreateFood'), 'method'=>'POST', 'enctype'=>'multipart/form-data')) }}
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Information</h3>
                </div>
                <div class="x_content">
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Category</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select name="CategoryId" class="form-control">
                                <option value="0">--- Select ---</option>
                                {!! $treeCategory !!}
                            </select>
                        </div>

                    </div>
                    <div class="form-group @if(count($errors->get('Title')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Title*</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input class="form-control title" type="text" name="Title" value="{{$data->Title}}"
                                   required>
                        </div>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group @if(count($errors->get('Description')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Description:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <textarea rows="3" class="form-control" type="text"
                                      name="Description">{{$data->Description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group @if(count($errors->get('Content')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Content:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <textarea rows="6" class="form-control editor" type="text"
                                      name="Content">{{$data->Content}}</textarea>
                        </div>
                        <input name="imageTemp" type="file" id="uploadImageContent" class="hidden" onchange="">
                    </div>
                    <div class="form-group @if(count($errors->get('Image')) > 0) has-error @endif  row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Image:</label>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <input id="inputImage" type="file" name="Image">
                            <div class="img-container">
                                <input type="hidden" name="avatar-data" value="{{$data->Image}}">
                                <img id="preview-avatar" width="300" height="300" class="image-cropping preview-avatar" src="{{$data->Image}}">
                            </div>

                        </div>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="row form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12">Active: <input @if($data->IsActive == 1) checked
                                                                                    @endif type="radio" name="IsActive"
                                                                                    value="1"></label>
                        <label class="col-md-12 col-sm-12 col-xs-12">Not Active: <input
                                    @if($data->IsActive == 0) checked @endif type="radio" name="IsActive"
                                    value="0"></label>
                    </div>
                </div>


            </div>

        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Option</h3>
                </div>
                <div class="x_content">
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Homepage: <input
                                    @if($data->IsHomepage == 1) checked
                                    @endif type="radio" name="IsHomepage"
                                    value="1"></label>
                        <label class="col-md-12 col-sm-12 col-xs-12">Not Homepage: <input
                                    @if($data->IsHomepage == 0) checked @endif type="radio" name="IsHomepage"
                                    value="0"></label>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Hot: <input @if($data->IsHot == 1) checked
                                                                                 @endif type="radio" name="IsHot"
                                                                                 value="1"></label>
                        <label class="col-md-12 col-sm-12 col-xs-12">Not Hot: <input
                                    @if($data->IsHot == 0) checked @endif type="radio" name="IsHot"
                                    value="0"></label>
                    </div>
                    <div class="row ln_solid"></div>
                    <div class="form-group row">
                        <label class="col-md-12 col-sm-12 col-xs-12">Stock: <input @if($data->IsStock == 1) checked
                                                                                   @endif type="radio" name="IsStock"
                                                                                   value="1"></label>
                        <label class="col-md-12 col-sm-12 col-xs-12">Not Stock: <input
                                    @if($data->IsStock == 0) checked @endif type="radio" name="IsStock"
                                    value="0"></label>
                    </div>
                </div>


            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <input class="btn btn-success" type="submit" name="submit" value="Save">
            <input class="btn btn-danger" type="reset" value="Reset">
            <a class="btn btn-info" href="{{route('ListFood')}}">Return</a>
        </div>
        {{ Form::close() }}
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview-avatar').attr('src', e.target.result);
                        $('#preview-avatar').show();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#inputImage").change(function () {
                readURL(this);
            });
        })
    </script>
@endsection