@extends('admin.layout.admin_layout')
@section('title', 'Food Manager')

@section('content')


        <!-- top tiles -->
        <div class="row tile_count">
                <h2>List Food</h2>
                <a class="btn btn-primary" href="{{route('CreateFood')}}">Add a new food</a>
        </div>
        <!-- /top tiles -->

        <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                                <div class="x_title">@include('admin.shared._alert')</div>
                                <div class="x_content">
                                        <div class="form-group row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <table class="table table-bordered">
                                                                <thead>
                                                                <tr>
                                                                        <th>#</th>
                                                                        <th>Image</th>
                                                                        <th>Title</th>
                                                                        <th>Description</th>
                                                                        <th>Created At</th>
                                                                        <th>Active</th>
                                                                        <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($listFood as $row)
                                                                        <tr>
                                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                                <th>{{asset($row->Image)}}</th>
                                                                                <td>{{$row->Title}}</td>
                                                                                <td>{{$row->Description}}</td>
                                                                                <td>{{$row->CreatedAt->format('d/m/Y')}}</td>
                                                                                <td>@if($row->IsActive == true) <span
                                                                                                class="btn btn-success">Active</span> @else <span
                                                                                                class="btn btn-danger">Not Active</span>  @endif</td>
                                                                                <td>

                                                                                        <a class="btn btn-info" href="{{route('UpdateFoodCategory',['id'=>$row->Id])}}">Update</a>
                                                                                        <a class="btn btn-warning btn-delete" href="{{route('DeleteFoodCategory',['id'=>$row->Id])}}">Delete</a>
                                                                                </td>
                                                                        </tr>
                                                                @endforeach
                                                                </tbody>
                                                        </table>
                                                        {{ $listFood->links() }}
                                                </div>
                                        </div>

                                </div>
                        </div>
                </div>
        </div>

@endsection

@section('js')

@endsection