<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="{{url('panel')}}" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{asset('admin/images/img.jpg')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>Charlie</h2>
            </div>
        </div>
        <!-- /menu prile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a href="{{url('panel')}}"><i class="fa fa-home"></i> Home </a>
                    </li>
                    <li><a><i class="fa fa-address-card" aria-hidden="true"></i> Administrator <span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href=" {{route('ListAdmin')}}">List</a>
                            </li>
                            <li><a href="{{route('ListGroupAdmin')}}">Group Administrator</a>
                            </li>
                            <li><a href="{{route('ListModule')}}">Module</a>
                            </li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-newspaper-o" aria-hidden="true"></i> Article <span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="general_elements.html">General Elements</a>
                            </li>
                            <li><a href="media_gallery.html">Media Gallery</a>
                            </li>
                            <li><a href="typography.html">Typography</a>
                            </li>
                            <li><a href="icons.html">Icons</a>
                            </li>
                            <li><a href="glyphicons.html">Glyphicons</a>
                            </li>
                            <li><a href="widgets.html">Widgets</a>
                            </li>
                            <li><a href="invoice.html">Invoice</a>
                            </li>
                            <li><a href="inbox.html">Inbox</a>
                            </li>
                            <li><a href="calender.html">Calender</a>
                            </li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-user-circle-o" aria-hidden="true"></i> Employee <span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="tables.html">Employee</a>
                            </li>
                            <li><a href="tables_dynamic.html">Shiper</a>
                            </li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-glass" aria-hidden="true"></i>Food <span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="{{route('ListFood')}}">List</a>
                            </li>
                            <li><a href="{{route('ListFoodCategory')}}">Category</a>
                            </li>
                            <li><a href="{{route('ScheduleFood')}}">Schedule</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-list-alt" aria-hidden="true"></i> Order <span
                                    class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="e_commerce.html">E-commerce</a>
                            </li>
                            <li><a href="projects.html">Projects</a>
                            </li>
                            <li><a href="project_detail.html">Project Detail</a>
                            </li>
                            <li><a href="contacts.html">Contacts</a>
                            </li>
                            <li><a href="profile.html">Profile</a>
                            </li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-table" aria-hidden="true"></i> TimeSheet</a>
                    </li>
                    <li><a><i class="fa fa-cogs" aria-hidden="true"></i> Config <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="e_commerce.html">General</a>
                            </li>
                            <li><a href="projects.html">SEO</a>
                            </li>
                            <li><a href="projects.html">Banner</a>
                            </li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-bar-chart" aria-hidden="true"></i> Statistic</a>
                    </li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>