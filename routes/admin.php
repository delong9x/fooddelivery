<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'admin'], function () {
    Route::get('/', 'PanelController@Index')->name('HomePanel');
    Route::get('/administrator', 'AdministratorController@Index')->name('ListAdmin');
    Route::get('/administrator/create', 'AdministratorController@Create')->name('CreateAdmin');
    Route::post('/administrator/create', 'AdministratorController@DoCreate')->name('DoCreateAdmin');
    Route::get('/administrator/{id}/update', 'AdministratorController@Update')->name('UpdateAdmin');
    Route::post('/administrator/{id}/update', 'AdministratorController@DoUpdate')->name('DoUpdateAdmin');
    Route::get('/administrator/{id}/delete', 'AdministratorController@Delete')->name('DeleteAdmin');

    Route::get('/food', 'FoodController@Index')->name('ListFood');
    Route::get('/food/create', 'FoodController@Create')->name('CreateFood');
    Route::post('/food/create', 'FoodController@DoCreate')->name('DoCreateFood');
    Route::get('/food/{id}/update', 'FoodController@Update')->name('UpdateFood');
    Route::post('/food/update', 'FoodController@DoUpdate')->name('DoUpdateFood');
    Route::get('/food/{id}/delete', 'FoodController@Delete')->name('DeleteFood');
    Route::get('/food/schedule', 'FoodController@Create')->name('ScheduleFood');

    Route::get('/food_category', 'FoodCategoryController@Index')->name('ListFoodCategory');
    Route::get('/food_category/create', 'FoodCategoryController@Create')->name('CreateFoodCategory');
    Route::post('/food_category/create', 'FoodCategoryController@DoCreate')->name('DoCreateFoodCategory');
    Route::get('/food_category/{id}/update', 'FoodCategoryController@Update')->name('UpdateFoodCategory');
    Route::post('/food_category/update', 'FoodCategoryController@DoUpdate')->name('DoUpdateFoodCategory');
    Route::get('/food_category/{id}/delete', 'FoodCategoryController@Delete')->name('DeleteFoodCategory');

    Route::get('groupadmin', 'GroupAdministratorController@Index')->name('ListGroupAdmin');

    Route::get('/module', 'ModuleController@Index')->name('ListModule');


});

Route::get('/login', 'Auth\LoginController@Index')->name('AdminLogin');
Route::post('/login', 'Auth\LoginController@DoLogin')->name('DoAdminLogin');